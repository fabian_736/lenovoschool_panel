<?php

use App\Http\Controllers\RutasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//RUTAS AUTH
Route::get('/reset',[RutasController::class,'reset'])->name('auth.reset');


//RUTAS HOME
Route::get('/home',[RutasController::class,'home'])->name('home.index');


//RUTAS USER
Route::get('/user/create',[RutasController::class,'user_create'])->name('user.create');
Route::get('/user/interaction',[RutasController::class,'user_interaction'])->name('user.interaction');
Route::get('/user/all',[RutasController::class,'user_all'])->name('user.all');


//RUTAS CHALLENGE
Route::get('/challenge/create',[RutasController::class,'challenge_create'])->name('challenge.create');
Route::get('/challenge/all',[RutasController::class,'challenge_all'])->name('challenge.all');


//RUTAS GIFT
Route::get('/gift/create',[RutasController::class,'gift_create'])->name('gift.create');
Route::get('/gift/follow',[RutasController::class,'gift_follow'])->name('gift.follow');
Route::get('/gift/all',[RutasController::class,'gift_all'])->name('gift.all');


//RUTAS CLASS
Route::get('/class/create',[RutasController::class,'class_create'])->name('class.create');
Route::get('/class/all',[RutasController::class,'class_all'])->name('class.all');


//RUTAS OTHER
Route::get('/other/ad',[RutasController::class,'other_ad'])->name('other.ad');
Route::get('/other/medal',[RutasController::class,'other_medal'])->name('other.medal');
Route::get('/other/tutorial',[RutasController::class,'other_tutorial'])->name('other.tutorial');


//RUTAS PROFILE
Route::get('/profile',[RutasController::class,'profile'])->name('profile.index');


//RUTAS CONFIG
Route::get('/config',[RutasController::class,'config'])->name('config.index');