<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RutasController extends Controller
{


    //RUTAS AUTH

    public function login(){
        return view('auth.login');
    }

    public function reset(){
        return view('auth.reset');
    }



    //RUTAS HOME

    public function home(){
        return view('home.index');
    }



    //RUTAS USER

    public function user_create(){
        return view('user.create');
    }

    public function user_interaction(){
        return view('user.interaction');
    }

    public function user_all(){
        return view('user.all');
    }



    //RUTAS CHALLENGE
    public function challenge_create(){
        return view('challenge.create');
    }

    public function challenge_all(){
        return view('challenge.all');
    }



    //RUTAS GIFT
    public function gift_create(){
        return view('gift.create');
    }

    public function gift_follow(){
        return view('gift.follow');
    }

    public function gift_all(){
        return view('gift.all');
    }



    //RUTAS CLASS
    public function class_create(){
        return view('class.create');
    }

    public function class_all(){
        return view('class.all');
    }



     //RUTAS OTHER
    public function other_ad(){
        return view('other.ad');
    }

    public function other_medal(){
        return view('other.medal');
    }

    public function other_tutorial(){
        return view('other.tutorial');
    }



     //RUTAS PROFiLE
     public function profile(){
         return view('profile.index');
     }


      //RUTAS CONFIG
      public function config(){
        return view('config.index');
    }
    

}
