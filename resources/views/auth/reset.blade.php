@extends('layouts.auth.app')
@section('content')


<div class="container my-auto">
    <div class="row">
      <div class="col-lg-4 col-md-8 col-12 mx-auto">
        <div class="card z-index-0 fadeIn3 fadeInBottom">
          <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
            <div class=" shadow-primary border-radius-lg py-3 pe-1" style="background: #25353f">
                <div class="col d-flex justify-content-center p-3">
                    <img src="{{url('img/logo_blanco.png')}}" alt="">
                </div>
            </div>
          </div>
          <div class="card-body">
            <form role="form" class="text-start">
              <div class="col">
                <label for="">Por favor escribe tu correo para poder recuperar tu cuenta</label>
              </div>
              <div class="input-group input-group-outline my-3">
                <label class="form-label">Correo electrónico</label>
                <input type="email" class="form-control">
              </div>
              <div class="text-center">
                <button type="button" class="btn w-100 my-4 mb-2 text-white" style="background: #7DBE38">Enviar codigo</button>
              </div>
              <p class="mt-4 text-sm text-center">
                <a href="{{ url()->previous() }}" class="text-dark text-gradient font-weight-bold"><i class="fas fa-undo-alt mx-1"></i>Volver al inicio</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

  
  @endsection