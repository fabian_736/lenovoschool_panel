@extends('layouts.content.app')
@section('content')


<div class="row-reverse">
    <div class="col card card-body">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
              <h6 class="text-white text-capitalize ps-3">Listado de todos los desafios</h6>
            </div>
        </div>
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Ciudad</th>                               
                    <th>Año de Ingreso</th>
                    <th>Salario</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Tiger Nixon</td>
                    <td>Arquitecto</td>
                    <td>Edinburgh</td>                                
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                    <td class="row mx-auto">
                        <div class="col d-flex justify-content-center">
                            <a href="javascript:;" class="" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <i class="fas fa-edit fa-2x"></i>
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center">
                            <a href="javascript:;" class="" onclick="undelete()">
                                <i class="fas fa-trash-alt fa-2x"></i>
                            </a>
                        </div>
                    </td>
                </tr>             
            </tbody>        
           </table>       
    </div>
</div>

         
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content ">
        <div class="modal-header">
          <h5 class="modal-title font-weight-normal" id="exampleModalLabel">Editar desafio</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
            <i class="far fa-times-circle text-dark"></i>
          </button>
        </div>
        <div class="modal-body p-5">
            <form action="">
                <div class="row">
                    <div class="col">
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">Nombre del desafio</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">etiqueta</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Fecha de vencimiento</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="date" class="form-control">
                        </div>
                    </div>
                    <div class="col">
                        <label class="form-label font-weight-bold">Puntos</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Subir imagen:</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="file" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <a href="javascript:;" onclick="update()" class="btn w-50 my-4 mb-2 text-white" style="background: #7DBE38">Actualizar desafio</a>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>

  <script>
    const update = () => {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Desafio actualizado',
            showConfirmButton: false,
            timer: 1500
        }).then(function() {
            window.location = "{{route('challenge.all')}}";
        });
    }

    const undelete = () => {
        Swal.fire({
            title: '¿Estas seguro que quieres eliminar este desafio?',
            text: "Eliminacion permanente",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire(
                'Eliminando!',
                'Este desafio ha sido eliminado.',
                'success'
                )
            }
        }).then(function() {
            window.location = "{{route('challenge.all')}}";
        });
    }
</script>
@endsection