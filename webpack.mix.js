const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);


    mix.styles([
        'resources/assets/css/material-dashboard.css',
    ], 'public/css/app.css')
    
        .scripts([
           'resources/assets/js/core/popper.min.js',
           'resources/assets/js/core/bootstrap.min.js',
           'resources/assets/js/plugins/perfect-scrollbar.min.js',
           'resources/assets/js/plugins/smooth-scrollbar.min.js',
           'resources/assets/js/plugins/chartjs.min.js',
           'resources/assets/js/material-dashboard.min.js'
           ], 'public/js/app.js');
