<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3" id="sidenav-main" style="background: #25353f !important">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0 d-flex justify-content-center" href="{{route('home.index')}}" target="_blank">
        <img src="{{url('img/logo_blanco.png')}}" class="navbar-brand-img h-100 mx-auto" alt="main_logo">
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white  {{ request()->is('home') ? 'active' : '' }}" href="{{route('home.index')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Inicio</span>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link text-white collapsed {{ request()->is('user/create', 'user/interaction', 'user/all') ? 'active' : '' }}" data-bs-toggle="collapse" aria-expanded="false" href="#profileExample1">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">table_view</i>
            </div>
            <span class="nav-link-text ms-1">Control de usuarios</span>
          </a>
          <div class="collapse" id="profileExample1" style="">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('user/create') ? 'active' : '' }}" href="{{route('user.create')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Crear usuario</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('user/interaction') ? 'active' : '' }}" href="{{route('user.interaction')}}">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal  ms-2  ps-1">Interacciones de usuario</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('user/all') ? 'active' : '' }}" href="{{route('user.all')}}">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal  ms-2  ps-1">Ver todos los usuarios</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link text-white collapsed {{ request()->is('challenge/create', 'challenge/all') ? 'active' : '' }}" data-bs-toggle="collapse" aria-expanded="false" href="#profileExample2">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">receipt_long</i>
            </div>
            <span class="nav-link-text ms-1">Desafios</span>
          </a>
          <div class="collapse" id="profileExample2" style="">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('challenge/create') ? 'active' : '' }}" href="{{route('challenge.create')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Crear desafios</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('challenge/all') ? 'active' : '' }}" href="{{route('challenge.all')}}">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal  ms-2  ps-1">Ver todos los desafios</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link text-white collapsed {{ request()->is('gift/create', 'gift/follow', 'gift/all') ? 'active' : '' }}" data-bs-toggle="collapse" aria-expanded="false" href="#profileExample3">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">receipt_long</i>
            </div>
            <span class="nav-link-text ms-1">Premios</span>
          </a>
          <div class="collapse" id="profileExample3" style="">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('gift/create') ? 'active' : '' }}" href="{{route('gift.create')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Crear premios</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('gift/follow') ? 'active' : '' }}" href="{{route('gift.follow')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Seguimiento de premios</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('gift/all') ? 'active' : '' }}" href="{{route('gift.all')}}">
                  <span class="sidenav-mini-icon"> A </span>
                  <span class="sidenav-normal  ms-2  ps-1">Ver todos los premios</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link text-white collapsed {{ request()->is('class/create', 'class/all') ? 'active' : '' }}" data-bs-toggle="collapse" aria-expanded="false" href="#profileExample4">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">receipt_long</i>
            </div>
            <span class="nav-link-text ms-1">Clases en vivo</span>
          </a>
          <div class="collapse" id="profileExample4" style="">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('class/create') ? 'active' : '' }}" href="{{route('class.create')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Crear clase en vivo</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('class/all') ? 'active' : '' }}" href="{{route('class.all')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Ver historial de clases</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link text-white collapsed {{ request()->is('other/ad', 'other/medal', 'other/tutorial') ? 'active' : '' }}" data-bs-toggle="collapse" aria-expanded="false" href="#profileExample5">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">receipt_long</i>
            </div>
            <span class="nav-link-text ms-1">Contenidos</span>
          </a>
          <div class="collapse" id="profileExample5" style="">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('other/ad') ? 'active' : '' }}" href="{{route('other.ad')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Anuncios</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('other/medal') ? 'active' : '' }}" href="{{route('other.medal')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Insignias</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('other/tutorial') ? 'active' : '' }}" href="{{route('other.tutorial')}}">
                  <span class="sidenav-mini-icon"> P </span>
                  <span class="sidenav-normal  ms-2  ps-1">Tutoriales</span>
                </a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Otras configuraciones</h6>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white {{ request()->is('profile') ? 'active' : '' }}" href="{{route('profile.index')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">person</i>
            </div>
            <span class="nav-link-text ms-1">Mi perfil</span>
          </a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link text-white {{ request()->is('config') ? 'active' : '' }}" href="{{route('config.index')}}">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">assignment</i>
            </div>
            <span class="nav-link-text ms-1">Configuracion de cuenta</span>
          </a>
        </li>
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-danger mt-4 w-100" href="{{URL::to('/')}}" type="button">Cerrar Sesión</a>
      </div>
    </div>
  </aside>