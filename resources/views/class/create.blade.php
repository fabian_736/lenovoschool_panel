@extends('layouts.content.app')
@section('content')


<div class="row-reverse">
    <div class="col card card-body">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-2">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
              <h6 class="text-white text-capitalize ps-3">Crear una clase en vivo</h6>
            </div>
        </div>
        <div class="row ">
            <div class="col my-5">
                <p class="lead">Recuerda que debes iniciar sesión con la cuenta verificada de Lenovo School en Vimeo. <br><br> Por favor, da click en <a onclick="vimeo()" href="javascript:;" class="text-dark font-weight-bold">VIMEO</a> para crear tu sesión en vivo <br>
                Una vez que hayas obtenido el link de tu transmicion, vuelve al panel y llena el siguiente formulario:</p>
            </div>
            <form action="">
                <div class="row">
                    <div class="col">
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">Pega aqui el link de la transmicion:</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">Nombre de la clase</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-group input-group-outline my-3">
                            <label class="form-label">Autor</label>
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Fecha de vencimiento</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="date" class="form-control">
                        </div>
                    </div>
                    <div class="col">
                        <label class="form-label font-weight-bold">Puntos</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Subir portada:</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="file" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <a href="javascript:;" onclick="confirm()" class="btn w-50 my-4 mb-2 text-white" style="background: #7DBE38">Crear clase en vivo</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    const vimeo = () => {
        
            window.open('https://vimeo.com/log_in', "_blank",
                "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=800,height=800");
  
    }

    const confirm = () => {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Desafio registrado',
            showConfirmButton: false,
            timer: 1500
        }).then(function() {
            window.location = "{{route('challenge.all')}}";
        });
    }
</script>


@endsection