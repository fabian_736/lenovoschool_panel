@extends('layouts.content.app')
@section('content')


<div class="row-reverse">
    <div class="col card card-body">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
              <h6 class="text-white text-capitalize ps-3">Seguimientos de premios</h6>
            </div>
        </div>
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Ciudad</th>                               
                    <th>Año de Ingreso</th>
                    <th>Salario</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Tiger Nixon</td>
                    <td>Arquitecto</td>
                    <td>Edinburgh</td>                                
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                    <td>
                        <a href="{{route('user.all')}}" class="d-flex justify-content-center" >
                            <i class="fas fa-eye fa-2x"></i>
                        </a>
                    </td>
                </tr>            
            </tbody>        
        </table>       
    </div>
</div>


@endsection