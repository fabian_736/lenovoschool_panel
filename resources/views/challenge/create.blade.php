@extends('layouts.content.app')
@section('content')


<div class="row-reverse">
    <div class="col card card-body">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
              <h6 class="text-white text-capitalize ps-3">Crear un desafio</h6>
            </div>
        </div>
        <form action="">
            <div class="row">
                <div class="col">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">Nombre del desafio</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="input-group input-group-outline my-3">
                        <label class="form-label">etiqueta</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label class="form-label font-weight-bold">Fecha de vencimiento</label>
                    <div class="input-group input-group-outline my-3">
                        <input type="date" class="form-control">
                    </div>
                </div>
                <div class="col">
                    <label class="form-label font-weight-bold">Puntos</label>
                    <div class="input-group input-group-outline my-3">
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label class="form-label font-weight-bold">Subir imagen:</label>
                    <div class="input-group input-group-outline my-3">
                        <input type="file" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col d-flex justify-content-center">
                    <a href="javascript:;" onclick="confirm()" class="btn w-50 my-4 mb-2 text-white" style="background: #7DBE38">Crear desafio</a>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    const confirm = () => {
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Desafio registrado',
            showConfirmButton: false,
            timer: 1500
        }).then(function() {
            window.location = "{{route('challenge.all')}}";
        });
    }
</script>


@endsection