
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{url('svg/icon_lenovo.svg')}}">
  <link rel="icon" type="image/png" href="{{url('svg/icon_lenovo.svg')}}">
  <title>
    Lenovo School | Panel Administrativo
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
 
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="{{url('css/app.css')}}" rel="stylesheet" />
  <!-- CSS personalizado --> 
  <link rel="stylesheet" href="{{url('main.css')}}">  
 <!--datables CSS básico-->
 <link rel="stylesheet" type="text/css" href="{{url('datatable/datatables/datatables.min.css')}}"/>
 <!--datables estilo bootstrap 4 CSS-->  
 <link rel="stylesheet"  type="text/css" href="{{url('datatable/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css')}}">
        


</head>

<body class="g-sidenav-show  bg-gray-200">