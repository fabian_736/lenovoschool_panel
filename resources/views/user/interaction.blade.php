@extends('layouts.content.app')
@section('content')


<div class="row-reverse">
    <div class="col card card-body">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
              <h6 class="text-white text-capitalize ps-3">Listado de todos las interacciones</h6>
            </div>
        </div>
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Puesto</th>
                    <th>Ciudad</th>                               
                    <th>Año de Ingreso</th>
                    <th>Salario</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Tiger Nixon</td>
                    <td>Arquitecto</td>
                    <td>Edinburgh</td>                                
                    <td>2011/04/25</td>
                    <td>$320,800</td>
                    <td>
                        <a href="javascript:;" class="d-flex justify-content-center" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            <i class="fas fa-eye fa-2x"></i>
                        </a>
                    </td>
                </tr>            
            </tbody>        
        </table>       
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content ">
        <div class="modal-header">
          <h5 class="modal-title font-weight-normal" id="exampleModalLabel">Detalles del usuario</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
            <i class="far fa-times-circle text-dark"></i>
          </button>
        </div>
        <div class="modal-body p-5">
            <div class="row d-flex justify-content-center">
                <div class="col-3 border-radius-lg bg-gradient-primary d-flex justify-content-center p-2">
                    <a href="javascript:;" onclick="item1()" class="text-white">Desafios realizados</a>
                </div>
                <div class="col-3 border-radius-lg bg-gradient-primary mx-auto d-flex justify-content-center p-2">
                    <a href="javascript:;" onclick="item2()" class="text-white">Clases visitadas</a>
                </div>
                <div class="col-3 border-radius-lg bg-gradient-primary d-flex justify-content-center p-2">
                    <a href="javascript:;" onclick="item3()" class="text-white">Insignias ganadas</a>
                </div>
            </div>
            <div class="row my-5 item1" style="display: none">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Puesto</th>
                            <th>Ciudad</th>                               
                            <th>Año de Ingreso</th>
                            <th>Salario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>Arquitecto</td>
                            <td>Edinburgh</td>                                
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                        </tr>            
                    </tbody>        
                </table>     
            </div>
            <div class="row my-5 item2" style="display: none">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre2222222</th>
                            <th>Puesto</th>
                            <th>Ciudad</th>                               
                            <th>Año de Ingreso</th>
                            <th>Salario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>Arquitecto</td>
                            <td>Edinburgh</td>                                
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                        </tr>            
                    </tbody>        
                </table>     
            </div>
            <div class="row my-5 item3" style="display: none">
                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre3333333</th>
                            <th>Puesto</th>
                            <th>Ciudad</th>                               
                            <th>Año de Ingreso</th>
                            <th>Salario</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tiger Nixon</td>
                            <td>Arquitecto</td>
                            <td>Edinburgh</td>                                
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                        </tr>            
                    </tbody>        
                </table>     
            </div>
        </div>
      </div>
    </div>
  </div>


  <script>
      const item1 = () =>{
          $('.item1').show();
          $('.item2').hide();
          $('.item3').hide();
      }

      const item2 = () =>{
          $('.item1').hide();
          $('.item2').show();
          $('.item3').hide();
      }

      const item3 = () =>{
          $('.item1').hide();
          $('.item2').hide();
          $('.item3').show();
      }
  </script>


@endsection