    <!-- Head -->
    @include('layouts.content.components.head')
    <!-- End Head -->

    <!-- Sidebar -->
    @include('layouts.content.components.sidebar')
    <!-- End Sidebar -->

    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

        <!-- Navbar -->
        @include('layouts.content.components.navbar')
        <!-- End Navbar -->
        
        <div class="container-fluid py-4">

            <div class="row p-3 mx-auto">

                <div class="col">

                    @yield('content')

                </div>

        </div>

            <!-- Footer -->
            @include('layouts.content.components.footer')
            <!-- End Footer -->

        </div>
    </main>

    <!-- Sidebar_Right -->
    @include('layouts.content.components.sidebar_right')
    <!-- End Sidebar_Right -->


    <!--  End HTML   -->
    @include('layouts.content.components.end')
  